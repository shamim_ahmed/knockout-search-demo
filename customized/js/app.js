var Artist = function(name, genre) {
  this.name = name;
  this.genre = genre;
  this.isIncluded = ko.observable(true);
};

var ViewModel = function(data) {
  var self = this;

  self.artists = data;
  self.searchInput = ko.observable();

  self.searchInput.subscribe(function(val) {
    filterArtists(val);
  });

  self.printSelection = function(selectedArtist) {
    console.log("name: " + selectedArtist.name);
    console.log("genre: " + selectedArtist.genre);
  };

  function filterArtists(val) {
    val = val.toLowerCase();

    self.artists.forEach(function(artist) {
      if (artist.name.toLowerCase().indexOf(val) != -1) {
        artist.isIncluded(true);
      } else {
        artist.isIncluded(false);
      }
    });
  }
};

$(function() {
  var dataArray = [];
  dataArray.push(new Artist('Yanni', 'New Age'));
  dataArray.push(new Artist('Yiruma', 'Classical'));
  dataArray.push(new Artist('Enya', 'Soft Rock'));

  var vm = new ViewModel(dataArray);
  ko.applyBindings(vm);
});
