var RandomTasks = {
  verbs: ["be","have","do","say","get","make","go","know","take","see","come","think","look","want","give","use","find","tell","ask","work","seem","feel","try","leave","call"],
  nouns: ["time","person","year","way","day","thing","man","world","life","hand","part","toy","eye","pet","place","work","week","case","point","government","company","number","group","problem","fact"],
  generate: generate
}

function generate(n) {
  var vlen = this.verbs.length,
      nlen = this.nouns.length,
      vrand =  Math.floor((Math.random() * vlen)),
      nrand =  Math.floor((Math.random() * nlen));
  return [this.verbs[vrand], 'the', this.nouns[nrand]].join(" ");
}