function ViewModel() {

  var self = this;

  /**
   * Public variables and functions
   */
  this.filterTaskInput = ko.observable();
  this.tasks = ko.observableArray();

  /**
   * KO Bindings
   */
  this.filterTaskInput.subscribe(function(newValue) {
    filterTasks(newValue);
  });

  function filterTasks(filter) {
    filter = filter.toLowerCase();
    self.tasks().forEach(function(task) {
      if (task.name().toLowerCase().indexOf(filter) > -1) {
        task.isFiltered(true);
      } else {
        task.isFiltered(false);
      }
    });
  }
}

function Task(name) {
  this.name = ko.observable(name);
  this.isFiltered = ko.observable(true);
}

/**
 * Initialize the view model and generate some random tasks
 */
var vm = new ViewModel();
ko.applyBindings(vm);
for (var i = 0; i < 5; i++) {
  vm.tasks.push(new Task(RandomTasks.generate()));
}
